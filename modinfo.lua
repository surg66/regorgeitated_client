name = "Re-Gorge-itated (Client)"
version = "1.0.4"
russian = russian or (language == "ru")
description = (russian and 
"Версия: "..version..[[

Дополнительный клиентский мод для мода Re-Gorge-itated!
Этот мод вернет ваше главное меню обратно в меню The Gorge, позволит вам быстро найти сервер с включенным модом Re-Gorge-itated и исправить ваши рецепты в книге рецептов.

Примечание: этот мод не требуется, чтобы играть в основной мод Re-Gorge-itated на серверах, это просто дополнительный мод, который вы можете включить, чтобы погрузится в атмосферу прошедшего события The Gorge.]]
or 
"Version: "..version..[[

The Add-on client mod for Re-Gorge-itated!
This client mod will revert your main menu back to the Gorge menu, allow you to quick match to a server with the main server mod enabled and fix up your recipe bookrecipes.

Note: This mod is not required to play the main Re-Gorge-itated mod on servers, it is simply an add-on mod you can enable to enhance your experience.]])

author = "Re-Gorge-itated team"
forumthread = ""
api_version_dst = 10
icon_atlas = "images/modicon.xml"
icon = "modicon.tex"
priority = 9
dst_compatible = true
all_clients_require_mod = false
client_only_mod = true

local workshop_mod = folder_name and folder_name:find("workshop-") ~= nil

if not workshop_mod then
	name = " [Git build] "..name
end
