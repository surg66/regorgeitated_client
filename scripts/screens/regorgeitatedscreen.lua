local ServerListingScreen = require "screens/regorgeitatedserverlistingscreen"
local AchievementsPopup = require "screens/redux/achievementspopup"
local GenericWaitingPopup = require "screens/redux/genericwaitingpopup"
local PopupDialogScreen = require "screens/redux/popupdialog"
local CompendiumScreen = require "screens/redux/compendiumscreen"
local Screen = require "widgets/screen"
local Widget = require "widgets/widget"
local Image = require "widgets/image"
local QuagmireBookWidget = require "widgets/redux/quagmire_book"
local TEMPLATES = require "widgets/redux/templates"
local QuickJoinScreen = require "screens/redux/quickjoinscreen"
local CloudServerSettingsPopup = require "screens/regorgeitatedscreenserverpopup"

local OnlineStatus = require "widgets/onlinestatus"
local PlayerSummaryScreen = require "screens/redux/playersummaryscreen"

require("constants")

local ReGorgeItatedScreen = Class(Screen, function(self, prev_screen, session_data)
	Screen._ctor(self, "ReGorgeItatedScreen")

	self.parent_screen = prev_screen

	self.session_data = session_data

	self:DoInit()

	self.default_focus = self.menu
	self.menu:SetFocus()
end)

local function PushWaitingPopup()
	local event_wait_popup = GenericWaitingPopup("ItemServerContactPopup", _G.STRINGS.UI.ITEM_SERVER.CONNECT, nil, false)
	TheFrontEnd:PushScreen(event_wait_popup)
	return event_wait_popup
end

function ReGorgeItatedScreen:DoInit()
	self.root = self:AddChild(TEMPLATES.ScreenRoot())
	self.fg = self:AddChild(TEMPLATES.ReduxForeground())
	self.bg_anim = self.root:AddChild(TEMPLATES.QuagmireAnim())
	self.title = self.root:AddChild(TEMPLATES.ScreenTitle("Re-Gorge-itated"))
	self.menu = self.root:AddChild(self:_MakeMenu())
	self.menu.reverse = true

	self.achievements = self.root:AddChild(Widget("achievements"))
	self.achievements.connecting = self.achievements:AddChild(Widget("connecting"))
	self.achievements.connecting.bg = self.achievements.connecting:AddChild(Image("images/quagmire_recipebook.xml", "quagmire_recipe_menu_bg.tex"))
	self.achievements.connecting.bg:ScaleToSize(900, 550)	
	self.achievements.connecting.sync_indicator = self.achievements.connecting:AddChild(Widget("sync_indicator"))
	self.achievements.connecting.sync_indicator:SetPosition(0, 0)

	local image = self.achievements.connecting.sync_indicator:AddChild(Image("images/avatars.xml", "loading_indicator.tex"))
	local function dorotate() image:RotateTo(0, -360, .75, dorotate) end
	dorotate()
	image:SetScale(.8)
	image:SetTint(unpack(_G.UICOLOURS.BROWN_MEDIUM))

	self.achievements:SetPosition(120, -40)
	self.scheduledopentab = 1
	self.festival_key = FESTIVAL_EVENTS.QUAGMIRE
	self.festival_season = GetFestivalEventSeasons(self.festival_key)

	PostProcessor:SetColourCubeData(0, "images/colour_cubes/quagmire_cc.tex", "images/colour_cubes/quagmire_cc.tex")
	PostProcessor:SetColourCubeData(1, "images/colour_cubes/quagmire_cc.tex", "images/colour_cubes/quagmire_cc.tex")
	PostProcessor:SetColourCubeLerp(0, 1)
	PostProcessor:SetColourCubeLerp(1, 0)

	if not TheInput:ControllerAttached() then
		self.back_button = self.root:AddChild(TEMPLATES.BackButton(function()
			self:StopMusic(true)
			TheFrontEnd:FadeBack()
		end))
	end

	wxputils.GetEventStatus(self.festival_key, self.festival_season, function(success)
		self.inst:DoTaskInTime(0, function()
			if success then
				self.achievements.connecting:Hide()
				self.achievements.book = self.achievements:AddChild(QuagmireBookWidget(self.menu, self.event_details, self.festival_season))
				self.achievements.book:SetPosition(0, 0)
				self.achievements.book:MoveToFront()

				if self.scheduledopentab > 1 then
					self.achievements.book.tabs[self.scheduledopentab].onclick()
				end

				local rank = wxputils.GetLevel(self.festival_key, self.festival_season)
				self.userprogress = self.root:AddChild(TEMPLATES.UserProgress(nil))
				self.userprogress.bar:Kill()
				self.userprogress.rank:SetRank(GetMostRecentlySelectedItem(Profile, "profileflair"), rank)
				self.userprogress:SetPosition(450, 320)
                self.userprogress.btn:SetOnClick(function()
                    if TheFrontEnd:GetIsOfflineMode() or not TheNet:IsOnlineMode() then
                        TheFrontEnd:PushScreen(PopupDialogScreen(STRINGS.UI.MAINSCREEN.OFFLINE, STRINGS.UI.MAINSCREEN.ITEMCOLLECTION_DISABLE, 
                            {
                                {text=STRINGS.UI.FESTIVALEVENTSCREEN.OFFLINE_POPUP_LOGIN, cb = function()
                                        SimReset()
                                    end},
                                {text=STRINGS.UI.FESTIVALEVENTSCREEN.OFFLINE_POPUP_BACK, cb=function() TheFrontEnd:PopScreen() end },
                            }))
                    else
                        self:StopMusic()
                        self:_FadeToScreen(CompendiumScreen, {Profile})
                    end
                end)
			end
		end, self)
	end)
end

function ReGorgeItatedScreen:_MakeMenu()
	self.tooltip = self.root:AddChild(TEMPLATES.ScreenTooltip())

	local button_browse = TEMPLATES.MenuButton(STRINGS.UI.REGORGESCREEN.BROWSE, function() self:OnBrowseButton() end, STRINGS.UI.REGORGESCREEN.TOOLTIP_BROWSE, self.tooltip)
	local button_recipes = TEMPLATES.MenuButton(STRINGS.UI.REGORGESCREEN.RECIPES, function() self:OnShowRecipesButton() end, STRINGS.UI.REGORGESCREEN.TOOLTIP_RECIPES, self.tooltip)
	local button_accomplishments = TEMPLATES.MenuButton(STRINGS.UI.REGORGESCREEN.ACCOMPLISHMENTS, function() self:OnShowAccomplishmentsButton() end, STRINGS.UI.REGORGESCREEN.TOOLTIP_ACCOMPLISHMENTS, self.tooltip)
    local button_quickmatch = TEMPLATES.MenuButton(STRINGS.UI.FESTIVALEVENTSCREEN.QUICKMATCH, function() self:OnQuickmatchButton() end, STRINGS.UI.FESTIVALEVENTSCREEN.TOOLTIP_QUICKMATCH, self.tooltip)
    local button_host = TEMPLATES.MenuButton(STRINGS.UI.FESTIVALEVENTSCREEN.HOST, function() self:OnHostButton() end, STRINGS.UI.FESTIVALEVENTSCREEN.TOOLTIP_HOST, self.tooltip)

	local menu_items = {
		{widget = button_accomplishments},
		{widget = button_recipes},
		{widget = button_browse},		
		{widget = button_host},		
        {widget = button_quickmatch},
	}

	local menu = self.root:AddChild(TEMPLATES.StandardMenu(menu_items, 38, nil, nil, true))

	return menu
end


local function CalcQuickJoinServerScoreForEvent(server)
	-- Return the score for the server.
	-- Highest scored servers will have the highest priority
	-- Return -1 to reject the server
	if (not server.has_password)										    -- not passworded
		and server.current_players < server.max_players						-- not full
		and server.allow_new_players										-- the match has not started
		and string.lower(server.mode) == "quagmire"							-- gorge game mode
		and (server.ping ~= nil and server.ping >= 0 and server.ping < 200)	-- filter out bad pings
	then
		local score = 0
		if server._has_character_on_server then		score = score + 100		end
		if server.friend_playing then				score = score + 4		end
		if server.belongs_to_clan then				score = score + 2		end

		score = score + server.current_players * 1.5
		score = score + (
                (server.ping < 50 and 5) or
                (server.ping < 100 and 3) or
                (server.ping < 150 and 1) or
                0
            )
		return score
	end

	return -1
end

function ReGorgeItatedScreen:OnHostButton()
    TheFrontEnd:PushScreen(CloudServerSettingsPopup(self, self.user_profile))
end

function ReGorgeItatedScreen:GetCharacterPortrait(slot)
    return "images/saveslot_portraits.xml", "unknown"
end

function ReGorgeItatedScreen:GetDayAndSeasonText(slot)
    return "Event: The Gorge"
end

function ReGorgeItatedScreen:GetPresetText(slot)
    return "Can you stand the heat in The Gorge?"
end

function ReGorgeItatedScreen:OnQuickmatchButton()
    TheFrontEnd:PushScreen(QuickJoinScreen(self, false, self.session_data, 
		"",
		CalcQuickJoinServerScoreForEvent,
		function() self:OnHostButton() end,
		function() self:OnBrowseButton() end))
end

function ReGorgeItatedScreen:OnShowScreensaverButton()
	if self.achievements ~= nil then
		self.achievements:Hide()
	end
end

function ReGorgeItatedScreen:OnShowRecipesButton()
	if self.achievements ~= nil then
		if self.achievements:IsVisible() and self.scheduledopentab == 1 then
			self.achievements:Hide()
		else
			self.achievements:Show()

			if self.achievements.book ~= nil then
				self.scheduledopentab = 1
				self.achievements.book.tabs[self.scheduledopentab].onclick()
			end
		end
	end
end

function ReGorgeItatedScreen:OnShowAccomplishmentsButton()
	if self.achievements ~= nil then
		if self.achievements:IsVisible() and self.scheduledopentab == 2 then
			self.achievements:Hide()
		else
			self.achievements:Show()

			if self.achievements.book ~= nil then
				self.scheduledopentab = 2
				self.achievements.book.tabs[self.scheduledopentab].onclick()
			end
		end
	end
end

function ReGorgeItatedScreen:OnBrowseButton()
	local filter_settings = {
		{ is_forced = true, name = "ISDEDICATED", data = "ANY" },
		{ is_forced = true, name = "GAMEMODE", data = "quagmire" },
		{ is_forced = true, name = "HASPVP", data = "ANY" },
		{ is_forced = true, name = "SEASON", data = "ANY" },
	}

	self:_FadeToScreen(ServerListingScreen, {filter_settings, nil, false, self.session_data, {mode = "quagmire", tags = "quagmire", intention = "any"}, ""})
end

function ReGorgeItatedScreen:_FadeToScreen(screen_type, data)
	self.last_focus_widget = TheFrontEnd:GetFocusWidget()
	self.menu:Disable()
	self.leaving = true

	TheFrontEnd:FadeToScreen( self, function()
		local screen = screen_type(self.user_profile, unpack(data))
		return screen
	end, nil)
end

local function OnStartMusic(inst, self, music)
	self.musictask = nil
	self.musicstarted = true
	TheFrontEnd:GetSound():PlaySound(music, "FEMusic")
end

function ReGorgeItatedScreen:StartMusic()
	local music = nil
	if music ~= nil then
		if not self.musicstarted and self.musictask == nil then
			self.musictask = self.inst:DoTaskInTime(1.25, OnStartMusic, self, music)
		end
	else
		self.parent_screen:StartMusic()
	end
end

function ReGorgeItatedScreen:StopMusic(going_back)
	local music = nil
	if music ~= nil then
		if self.musicstarted then
			self.musicstarted = false
			TheFrontEnd:GetSound():KillSound("FEMusic")
		elseif self.musictask ~= nil then
			self.musictask:Cancel()
			self.musictask = nil
		end
	else
		if not going_back then
			self.parent_screen:StopMusic()
		end
	end
end

function ReGorgeItatedScreen:OnBecomeActive()
	if self.popup_backout then
		return
	end
	
	ReGorgeItatedScreen._base.OnBecomeActive(self)

	if not self.shown then
		self:Show()
	end

	if self.last_focus_widget then
		self.menu:RestoreFocusTo(self.last_focus_widget)
	end

	self.leaving = nil
end

function ReGorgeItatedScreen:OnControl(control, down)
	if self.achievements.book ~= nil and self.achievements.book:OnControlTabs(control, down) then
		return true 
	end

	if ReGorgeItatedScreen._base.OnControl(self, control, down) then return true end

	if not down and control == CONTROL_CANCEL then
		self:StopMusic(true)
		TheFrontEnd:FadeBack()
		return true
	end
end

function ReGorgeItatedScreen:OnUpdate(dt)
	if self.achievements.book ~= nil and self.achievements.book.OnUpdate ~= nil then
		self.achievements.book:OnUpdate(dt)
	end
end

function ReGorgeItatedScreen:GetHelpText()
	local controller_id = TheInput:GetControllerID()
	local t = {}

	table.insert(t, TheInput:GetLocalizedControl(controller_id, CONTROL_CANCEL) .. " " .. STRINGS.UI.HELP.BACK)

	if self.achievements.book ~= nil and not self.achievements.book.focus then
		table.insert(t, self.achievements.book:GetHelpText())
	end

	return table.concat(t, "  ")
end

return ReGorgeItatedScreen
