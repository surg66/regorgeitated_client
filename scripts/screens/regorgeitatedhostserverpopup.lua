local GenericWaitingPopup = require "screens/redux/genericwaitingpopup"
local PopupDialogScreen = require "screens/redux/popupdialog"
local Text = require "widgets/text"

local phases =
{
    STRINGS.UI.REGORGESCREEN.SUBANDDOWNLOAD,
    STRINGS.UI.REGORGESCREEN.UPDATE,
    STRINGS.UI.REGORGESCREEN.CONNECTING,
}

local Customise = require "map/customise"
local Levels = require "map/levels"

local HostCloudServerPopup = Class(GenericWaitingPopup, function(self, prev_screen, name, description, password, claninfo)
    GenericWaitingPopup._ctor(self, "HostCloudServerPopup", " ")

	self.prev_screen = prev_screen
	
    local title = self.dialog.title.parent:AddChild(Text(self.dialog.title.font, self.dialog.title.size, nil, self.dialog.title.colour))
    title:SetPosition(self.dialog.title:GetPosition())
    title:SetHAlign(ANCHOR_MIDDLE)

    local wid = self.dialog.title:GetRegionSize()
    self.dialog.title:Kill()
    self.dialog.title = title

    title:SetTruncatedString(STRINGS.UI.FESTIVALEVENTSCREEN.HOST.." - "..name, wid, 53, true)

    self.status_msg = self.dialog:AddChild(Text(CHATFONT, 28, phases[1]))
    self.status_msg:SetRegionSize(530, 28)
    self.status_msg:SetPosition(0, 50)
		
	self.save_slot = ShardSaveGameIndex:GetNextNewSlot()
	
	LoadRegorgeitatedMod()

    self.subscribed = false
    self.updated = false
	self.created = false
	
    self.current_option_settings = {}
    self.slotoptions = {}
		
	self.gamemode = function()
		return "quagmire"
	end
	
	local options = ShardSaveGameIndex:GetSlotGenOptions(self.save_slot, "Master")
	if options == nil or GetTableSize(options) == 0 then
		self.slotoptions[self.save_slot] = self.slotoptions[self.save_slot] or {}
		local level_type = GetLevelType( self.gamemode )
		local presetdata = Levels.GetDefaultLevelData(level_type, "forest")
		self.slotoptions[self.save_slot] = presetdata
	else
		self.slotoptions[self.save_slot] = self.slotoptions[self.save_slot] or {}
		self.slotoptions[self.save_slot] = options
	end

	local level = self.slotoptions[self.save_slot]
	if level then
		self:LoadPreset(level.id)
		for option, value in pairs(level.overrides or {}) do
			self:SetTweak(1, option, value)
		end
	end
	
	local serverdata = self.prev_screen:GetServerData()
	ShardSaveGameIndex:SetSlotServerData(self.save_slot, serverdata)
	ShardSaveGameIndex:SetSlotEnabledServerMods(self.save_slot)
	
	local options = self:CollectOptions()
	ShardSaveGameIndex:SetSlotGenOptions(self.save_slot, "Master", options)
	ShardSaveGameIndex:Save()
	
    local sessionid = ""
	self.sec = 0
end)

function HostCloudServerPopup:LoadPreset(preset)
    local presetdata = Levels.GetDataForLevelID(preset)

	if presetdata == nil then
		print(string.format("WARNING! Could not load a preset with id %s, loading MOD_MISSING preset instead.", tostring(preset)))
		presetdata = Levels.GetDataForLevelID("MOD_MISSING")
	end

    self.current_option_settings = {}
    self.current_option_settings.preset = presetdata.id
    self.current_option_settings.tweaks = {}
end

function HostCloudServerPopup:SetTweak(level, option, value)
    local presetdata = Levels.GetDataForLevelID(self.current_option_settings.preset)
    if presetdata ~= nil and presetdata.overrides[option] ~= nil then
        if value == presetdata.overrides[option] then
            self.current_option_settings.tweaks[option] = nil
        else
            self.current_option_settings.tweaks[option] = value
        end
    else
        if value == Customise.GetLocationDefaultForOption(presetdata.location, option) then
            self.current_option_settings.tweaks[option] = nil
        else
            self.current_option_settings.tweaks[option] = value
        end
    end
end

function HostCloudServerPopup:GetValueForOption(level, option)
    local presetdata = Levels.GetDataForLevelID(self.current_option_settings.preset)
    return Customise.GetLocationDefaultForOption(presetdata.location, option)
end

function HostCloudServerPopup:CollectOptions()
    local ret = nil
    local level_index,level = 1, self.current_option_settings
    if level then
		local preset = level.preset
        ret = Levels.GetDataForLevelID(preset)
        local options = Customise.GetOptionsWithLocationDefaults(Levels.GetLocationForLevelID(preset), level_index == 1)
        for i,option in ipairs(options) do
            ret.overrides[option.name] = self:GetValueForOption(level_index, option.name)
        end
    end

    return ret
end

function HostCloudServerPopup:OnUpdate(dt)
    HostCloudServerPopup._base.OnUpdate(self, dt)
	local modinfo = KnownModIndex:GetModInfo(ReGorgeItatedMod)
	if modinfo == nil then
		if not self.subscribed then
			TheSim:SubscribeToMod(ReGorgeItatedMod)
			TheSim:UpdateWorkshopMod(ReGorgeItatedMod)
			self.subscribed = true
		end
		self.status_msg:SetString(phases[1] or "")
	elseif IsWorkshopMod(ReGorgeItatedMod) and modinfo.version ~= "" and modinfo.version ~= TheSim:GetWorkshopVersion(tostring(ReGorgeItatedMod)) then
		if not self.updated then
			TheSim:UpdateWorkshopMod(ReGorgeItatedMod)
			self.updated = true
		end
		self.status_msg:SetString(phases[2] or "")
	else
		if not self.created then
			self:Create()
			self.created = true
		end
		self.status_msg:SetString(phases[3] or "")
	end
	KnownModIndex:UpdateSingleModInfo(ReGorgeItatedMod)
end

local function BuildTagsStringHosting(self, worldoptions)
	local serverdata = self.prev_screen:GetServerData()
    local tagsTable = {}

    table.insert(tagsTable, GetGameModeTag(serverdata.game_mode))

    if tostring(serverdata.pvp) then
        table.insert(tagsTable, STRINGS.TAGS.PVP)
    end

    if serverdata.privacy_type == PRIVACY_TYPE.FRIENDS then
        table.insert(tagsTable, STRINGS.TAGS.FRIENDSONLY)
    elseif serverdata.privacy_type == PRIVACY_TYPE.CLAN then
        table.insert(tagsTable, STRINGS.TAGS.CLAN)
    elseif serverdata.privacy_type == PRIVACY_TYPE.LOCAL then
        table.insert(tagsTable, STRINGS.TAGS.LOCAL)
    end

    local worlddata = worldoptions[1]
    if worlddata ~= nil and worlddata.location ~= nil then
        local locationtag = STRINGS.TAGS.LOCATION[string.upper(worlddata.location)]
        if locationtag ~= nil then
            table.insert(tagsTable, locationtag)
        end
    end

    return BuildTagsStringCommon(tagsTable)
end

function HostCloudServerPopup:Create()
    local function onCreate()
        local serverdata = self.prev_screen:GetServerData()
        local worldoptions = {}
        local specialeventoverride = nil
		worldoptions = self:CollectOptions()

        local world1datastring = ""
        if worldoptions ~= nil then
            local world1data = worldoptions
            world1datastring = DataDumper(world1data, nil, false)
        end

		UnLoadRegorgeitatedMod(self.save_slot)
        local cluster_info = {}

        local mod_data = DataDumper(ShardSaveGameIndex:GetSlotEnabledServerMods(self.save_slot), nil, false)
        print("V v v v v v v v v v v v v v v v")
        print(mod_data)
        print("^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^")
        cluster_info.mods_config                             = mod_data
        cluster_info.world1gen                               = world1datastring
        cluster_info.world2gen                               = ""
        cluster_info.friends_only                            = serverdata.privacy_type == PRIVACY_TYPE.FRIENDS

        cluster_info.settings                                = {}
        cluster_info.settings.NETWORK                        = {}
        cluster_info.settings.NETWORK.cluster_name           = serverdata.name
        cluster_info.settings.NETWORK.cluster_password       = serverdata.password
        cluster_info.settings.NETWORK.cluster_description    = serverdata.description
        cluster_info.settings.NETWORK.lan_only_cluster       = tostring(serverdata.privacy_type == PRIVACY_TYPE.LOCAL)
        cluster_info.settings.NETWORK.cluster_intention      = serverdata.intention
        cluster_info.settings.NETWORK.offline_cluster        = tostring(not serverdata.online_mode)
        cluster_info.settings.NETWORK.cluster_language       = LOC.GetLocaleCode()

        cluster_info.settings.GAMEPLAY                       = {}
        cluster_info.settings.GAMEPLAY.game_mode             = serverdata.game_mode
        cluster_info.settings.GAMEPLAY.pvp                   = tostring(serverdata.pvp)

        local gamemode_max_players = GetGameModeMaxPlayers(serverdata.game_mode)
        cluster_info.settings.GAMEPLAY.max_players           = tostring(gamemode_max_players ~= nil and math.min(serverdata.max_players, gamemode_max_players) or serverdata.max_players)

        if serverdata.privacy_type == PRIVACY_TYPE.CLAN then
            cluster_info.settings.STEAM                      = {}
            cluster_info.settings.STEAM.steam_group_only     = tostring(serverdata.clan.only)
            cluster_info.settings.STEAM.steam_group_id       = tostring(serverdata.clan.id)
            cluster_info.settings.STEAM.steam_group_admins   = tostring(serverdata.clan.admin)
        end

        local function onsaved()
            self:Disable()

            local encode_user_path = serverdata.encode_user_path == true
            local use_legacy_session_path = serverdata.use_legacy_session_path == true
            local launchingServerPopup = nil

            if not TheSystemService:StartDedicatedServers(self.save_slot, false, cluster_info, encode_user_path, use_legacy_session_path) then
                if launchingServerPopup ~= nil then
                    launchingServerPopup:SetErrorStartingServers()
                end
                self:Enable()
            else
                local function onsaved()
                    ShardSaveGameIndex.slot_cache[self.save_slot] = nil
                    assert(ShardSaveGameIndex:GetShardIndex(self.save_slot, "Master"), "failed to save shardindex.")

                    TheNet:SetServerTags(BuildTagsStringHosting(self, worldoptions))
                    DoLoadingPortal(function()
                        StartNextInstance({ reset_action = RESET_ACTION.LOAD_SLOT, save_slot = self.save_slot })
                    end)
                end
                local masterShardGameIndex = ShardSaveGameIndex:GetShardIndex(self.save_slot, "Master", true)
                local defaultserverdata = GetDefaultServerData()
                defaultserverdata.encode_user_path = encode_user_path
                defaultserverdata.use_legacy_session_path = use_legacy_session_path
                masterShardGameIndex:SetServerShardData(nil, defaultserverdata, onsaved)
            end
        end

        if ShardSaveGameIndex:IsSlotEmpty(self.save_slot) then
            local starts = Profile:GetValue("starts") or 0
            Profile:SetValue("starts", starts + 1)
            Profile:Save(onsaved)
        else
            onsaved()
        end
    end
	onCreate()
end

function HostCloudServerPopup:OnError(body)
    self:Disable()
    self:StopUpdating()

    TheFrontEnd:PushScreen(PopupDialogScreen(
        STRINGS.UI.FESTIVALEVENTSCREEN.HOST_FAILED,
        body or STRINGS.UI.FESTIVALEVENTSCREEN.HOST_FAILED_BODY,
        { { text = STRINGS.UI.POPUPDIALOG.OK, cb = function() TheFrontEnd:PopScreen() end } }
    ))
    TheFrontEnd:PopScreen(self)
end

function HostCloudServerPopup:OnCancel()
	self.sec = 0
    HostCloudServerPopup._base.OnCancel(self)
end

return HostCloudServerPopup
