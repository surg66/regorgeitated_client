Assets =
{
	Asset("ATLAS", "images/regorge_frontend.xml"),
	Asset("IMAGE", "images/regorge_frontend.tex")
}

local _G = GLOBAL
local KnownModIndex = _G.KnownModIndex
local ModManager = _G.ModManager
local Profile = _G.Profile
local require = _G.require
local STRINGS = _G.STRINGS

STRINGS.UI.REGORGESCREEN =
{
	ENTER = "Enter Re-Gorge-itated",
	BROWSE = "Browse Servers",
	RECIPES = "Recipe Book",
	ACCOMPLISHMENTS = "Accomplishments",
	TOOLTIP_BROWSE = "Browse all Re-gorge-itated servers",
	TOOLTIP_RECIPES = "See what's in your recipe book",
	TOOLTIP_ACCOMPLISHMENTS = "Your Gorge Accomplishments",
	SUBANDDOWNLOAD = "Subscribing and Downloading mod",
	UPDATE = "Updating mod",
	CONNECTING = "Connecting",
}

local ReGorgeItatedScreen = require "screens/regorgeitatedscreen"
local Widget = require "widgets/widget"
local Image = require "widgets/image"
local Text = require "widgets/text"
local UIAnim = require "widgets/uianim"
local TEMPLATES = require "widgets/redux/templates"
local PopupDialogScreen = require "screens/redux/popupdialog"
local CompendiumScreen = require "screens/redux/compendiumscreen"

_G.ReGorgeItatedMod = "workshop-1918927570"

local recipesdata = require "gorge_recipesdata"
_G.LoadRegorgeitatedMod = function()
	TheSim:UnlockModDir()
	KnownModIndex:Enable(_G.ReGorgeItatedMod)
	ModManager:FrontendLoadMod(_G.ReGorgeItatedMod)
	KnownModIndex:SetDependencyList(_G.ReGorgeItatedMod, KnownModIndex:GetModDependencies(_G.ReGorgeItatedMod))
    local prefabs_to_load = {}
	local prefab = Prefab("MODSCREEN_".._G.ReGorgeItatedMod, nil, nil, nil)
	RegisterPrefabs(prefab)
	table.insert(prefabs_to_load, prefab.name)
    TheSim:LoadPrefabs(prefabs_to_load)
end

_G.UnLoadRegorgeitatedMod = function(slot)
	KnownModIndex:Save()
	Profile:Save()
	_G.ShardSaveGameIndex:SetSlotEnabledServerMods( slot )
	_G.ShardSaveGameIndex:Save()
	_G.TheFrontEnd:Fade(FADE_OUT, SCREEN_FADE_TIME, function()
		local prefabs_to_unload = {}
		table.insert(prefabs_to_unload, "MODSCREEN_".._G.ReGorgeItatedMod)
		TheSim:UnloadPrefabs(prefabs_to_unload)
		TheSim:UnregisterPrefabs(prefabs_to_unload)
		ForceAssetReset()
		SimReset()
	end)
end
_G.WORLD_FESTIVAL_EVENT = _G.FESTIVAL_EVENTS.QUAGMIRE

mods = _G.rawget(_G, "mods") or (function() local m = {}_G.rawset(_G, "mods", m) return m end)()

if mods.RussianLanguagePack or mods.UniversalTranslator then
	STRINGS.UI.REGORGESCREEN.ENTER = "Вход в Re-Gorge-itated"
	STRINGS.UI.REGORGESCREEN.BROWSE = "Просмотреть игры"
	STRINGS.UI.REGORGESCREEN.RECIPES = "Книга рецептов"
	STRINGS.UI.REGORGESCREEN.ACCOMPLISHMENTS = "Достижения"
	STRINGS.UI.REGORGESCREEN.TOOLTIP_BROWSE = "Просмотреть все Re-gorge-itated игры"
	STRINGS.UI.REGORGESCREEN.TOOLTIP_RECIPES = "Посмотрите, что есть в вашей книге рецептов"
	STRINGS.UI.REGORGESCREEN.TOOLTIP_ACCOMPLISHMENTS = "Ваши Достижения в The Gorge"
	STRINGS.UI.REGORGESCREEN.SUBANDDOWNLOAD = "Подписываемся и устанавливаем мод"
	STRINGS.UI.REGORGESCREEN.UPDATE = "Обновляем мод"
	STRINGS.UI.REGORGESCREEN.CONNECTING = "Подключаемся"
end

_G.FE_MUSIC = _G.FESTIVAL_EVENT_MUSIC.quagmire.sound

local function FillTableCoins(coinstable, coins)
	if coins ~= nil then
		local levels = 0
		local max_coin = 1

		for _, v in pairs(coins) do
			levels = levels + 1
			if v > 0 then
				max_coin = levels
			end
		end

		levels = 0
		for _, v in pairs(coins) do
			levels = levels + 1
			if levels <= max_coin then
				coinstable["coin"..levels] = v
			else
				coinstable["coin"..levels] = nil
			end
		end
	end
end

local function SyncRecipe(book, name, festival_key, festival_season, session, date)
	local namerecipe = "quagmire_"..name
	if name == "food_syrup" then
		namerecipe = "quagmire_syrup"
	end
	local food_unlocked = _G.EventAchievements:IsAchievementUnlocked(festival_key, festival_season, name)

	if food_unlocked and book.recipes[namerecipe] == nil then
		local data = recipesdata[name]

		if namerecipe == "quagmire_syrup" then
			book.recipes["quagmire_syrup"] = {
				station = data.stations,
				size = data.size,
				new = "new",
				date = date,
				recipes = data.ingredients,
				session = session }
		else
			local base_coins = {}
			local silver_coins = {}

			FillTableCoins(base_coins, data.coins)
			FillTableCoins(silver_coins, data.silver_coins)

			book.recipes[namerecipe] = {
				dish = data.dish,
				base_value = base_coins,
				silver_value = silver_coins,
				tags = data.cravings,
				station = data.stations,
				size = data.size,
				new = "new",
				date = date,
				recipes = data.ingredients,
				session = session}
		end

		print("[TheRecipeBook] Added unlocked recipe:", namerecipe)
		return true
	end

	return false
end

AddGlobalClassPostConstruct("quagmire_recipebook", "TheRecipeBook", function(book)
	book.GetValidRecipes = function(self)
		local ret = {}
		local festival_key = _G.FESTIVAL_EVENTS.QUAGMIRE
		local festival_season = _G.GetFestivalEventSeasons(festival_key)

		-- Synchronization
		if festival_season == 1 then
			local count = 0

			for _, _ in pairs(self.recipes) do
				count = count + 1
			end

			if count < 70 then
				print("[TheRecipeBook] Synchronization unlocked recipes", count)
				local save_recipebook = false
				local date = _G.os.date("%d/%m/%Y/%X")
				local session = _G.TheNet:GetSessionIdentifier()
				if session == "" then session = "2CB154A871CA4B25" end -- fake session for menu

				for i = 1 , 69 do
					local food_name = "food_0"..i;
					if i < 10 then 
						food_name = "food_00"..i;
					end
					
					if SyncRecipe(self, food_name, festival_key, festival_season, session, date) then
						save_recipebook = true
					end
				end

				if SyncRecipe(self, "food_syrup", festival_key, festival_season, session, date) then
					save_recipebook = true
				end

				if save_recipebook then
					_G.TheSim:SetPersistentString("recipebook", _G.json.encode(self.recipes), false)
				end
			end
		end

		for k, v in pairs(self.recipes) do
			ret[k] = v
		end

		return ret
	end

	book.IsRecipeUnlocked = function(self, product)
		for k, v in pairs(self.recipes) do
			if tostring(k) == tostring(product) then
				return true
			end
		end

		return false
	end
end)

AddClassPostConstruct("screens/redux/mainscreen", function(self)
	_G.TheSim:LoadPrefabs({"quagmire_fest_frontend"})
end)

AddClassPostConstruct("screens/redux/multiplayermainscreen", function(self)
	self.banner_root:KillAllChildren()
	self.banner_root.anim_bg = self.banner_root:AddChild(UIAnim())

	local anims = {"quagmire_menu_bg", "quagmire_menu_mid", "quagmire_menu"}
	for _,anim in ipairs(anims) do
		local a = self.banner_root.anim_bg:AddChild(UIAnim())
		a:GetAnimState():SetBuild(anim)
		a:GetAnimState():SetBank(anim)
		a:SetScale(.667)
		a:SetPosition(0, 0)
		if anim == "quagmire_menu_bg" then
			a:GetAnimState():SetMultColour( .3, .3, .3, 1)
		end
		a:GetAnimState():PlayAnimation("idle", true)
	end

	self.banner_root.anim_bg:MoveToBack()

    self.logo:Kill()
	self.logo = self.fixed_root:AddChild(Image("images/regorge_frontend.xml", "regorge_frontend.tex"))	
	self.logo:SetScale(.6)
	self.logo:SetPosition(-_G.RESOLUTION_X/2 + 180, _G.RESOLUTION_Y / 2 - 170)
	self.logo:SetTint(_G.unpack(_G.FRONTEND_TITLE_COLOUR))
	if self.userprogress then
    	self.userprogress.bar:Kill()
		self.userprogress.btn:SetOnClick(function()
			if _G.TheFrontEnd:GetIsOfflineMode() or not _G.TheNet:IsOnlineMode() then
				_G.TheFrontEnd:PushScreen(PopupDialogScreen(STRINGS.UI.MAINSCREEN.OFFLINE, STRINGS.UI.MAINSCREEN.ITEMCOLLECTION_DISABLE, 
					{
						{text=STRINGS.UI.FESTIVALEVENTSCREEN.OFFLINE_POPUP_LOGIN, cb = function()
								SimReset()
							end},
						{text=STRINGS.UI.FESTIVALEVENTSCREEN.OFFLINE_POPUP_BACK, cb=function() TheFrontEnd:PopScreen() end },
					}))
			else
				self:StopMusic()
				self:_FadeToScreen(CompendiumScreen, {Profile})
			end
		end)
	end
	self.OnReGorgeItatedButton = function()
		if _G.TheFrontEnd:GetIsOfflineMode() or not _G.TheNet:IsOnlineMode() then
			_G.TheFrontEnd:PushScreen(PopupDialogScreen(STRINGS.UI.FESTIVALEVENTSCREEN.OFFLINE_POPUP_TITLE, STRINGS.UI.FESTIVALEVENTSCREEN.OFFLINE_POPUP_BODY["quagmire"], 
				{
					{text = STRINGS.UI.FESTIVALEVENTSCREEN.OFFLINE_POPUP_LOGIN, cb = function() _G.SimReset() end},
					{text = STRINGS.UI.FESTIVALEVENTSCREEN.OFFLINE_POPUP_BACK, cb = function() _G.TheFrontEnd:PopScreen() end},
				}))
		else
			self.last_focus_widget = _G.TheFrontEnd:GetFocusWidget()
			self.menu:Disable()
			self.leaving = true

			_G.TheFrontEnd:Fade(_G.FADE_OUT, _G.SCREEN_FADE_TIME, function()
				if fadeout_cb ~= nil then
					fadeout_cb()
				end				
				_G.TheFrontEnd:PushScreen(ReGorgeItatedScreen(self, self.session_data))
				_G.TheFrontEnd:Fade(_G.FADE_IN, _G.SCREEN_FADE_TIME)
				self:Hide()
			end)
		end
	end

	local lastitemmenu = #self.menu.items
	self.menu:EditItem(lastitemmenu, STRINGS.UI.REGORGESCREEN.ENTER, nil, self.OnReGorgeItatedButton)
	self.menu_root:SetPosition(0,-115)
	self.submenu:SetPosition(-_G.RESOLUTION_X*.5 + 90, -(_G.RESOLUTION_Y*.5)+65, 0)
end)

AddClassPostConstruct("screens/redux/mysteryboxscreen", function(self)
	self.userprogress:Kill() 
	local no_chest_msg = STRINGS.UI.MYSTERYBOXSCREEN.OUT_OF_BOXES_BODY_NO_EVENT
	self.alloutofbox:Kill() 
    self.alloutofbox = self.root:AddChild(TEMPLATES.CurlyWindow(400,100, nil, nil, nil, no_chest_msg))
    self.alloutofbox:SetPosition(0, -20)
end)


AddClassPostConstruct("screens/viewplayersmodalscreen", function(self)
	if _G.TheNet:GetIsServer() then
		if _G.TheNet:GetServerIsClientHosted() then
			_G.wxputils.GetEventStatus("quagmire", 1, function(success)
			end)
		end
	else
		_G.wxputils.GetEventStatus("quagmire", 1, function() end)
	end
end)
